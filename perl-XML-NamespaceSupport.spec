%define mod_name XML-NamespaceSupport

Name:           perl-%{mod_name}
Version:        1.12
Release:        9
Summary:        A simple generic namespace processor 
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
Url:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/P/PE/PERIGRIN/%{mod_name}-%{version}.tar.gz

BuildRequires:  make perl-generators perl-interpreter
BuildRequires:  perl(:VERSION) >= 5.6 perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(warnings) perl(constant) perl(vars) perl(Test::More)

BuildArch:      noarch

%description
This module offers a simple to process namespaced XML names (unames)
from within any application that may need them. It also helps maintain
a prefix to namespace URI map, and provides a number of basic checks.

%package_help

%prep
%autosetup -p1 -n %{mod_name}-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/XML/

%files help
%doc Changes README
%{_mandir}/man3/*.3*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.12-9
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 hongjinghao<hongjinghao@huawei.com> - 1.12-8
- add mod_name macro

* Fri Nov 15 2019 caomeng<caomeng5@huawei.com> - 1.12-7
- Package init
